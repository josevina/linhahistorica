# LinhaHistórica

## Sobre o projeto: 
**Linha histórica é um projeto de mapa mental com linha do tempo interativa, de várias ramificações e áreas da história, com informações verificadas**.


### Contribua se você sabe um pouco de programação front-end. Não se esqueça de seguir a base do projeto:   [canva](https://www.canva.com/design/DAFvCby9fx8/wT5qfNo2ijJQHl6YPsVk-Q/edit?utm_content=DAFvCby9fx8&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)
